DROP DATABASE IF EXISTS ejercicio2Ricardo;
CREATE DATABASE ejercicio2Ricardo;

USE ejercicio2Ricardo;

/* Creo las tablas*/

-- tabla Clientes

CREATE OR REPLACE TABLE clientes(
  id_cliente int AUTO_INCREMENT,
  nombre varchar(15),
  PRIMARY KEY(id_cliente));

-- tabla Productos

CREATE OR REPLACE TABLE productos(
  id_producto int AUTO_INCREMENT,
  nombre_producto varchar(25),
  peso decimal(5,2),
  fecha date,
  cantidad int(3),
  id_cliente int,
  PRIMARY KEY (id_producto));

/* A�ado las restricciones*/

ALTER TABLE productos
  ADD CONSTRAINT fk_productos_clientes
  FOREIGN KEY (id_cliente)
  REFERENCES clientes (id_cliente);

